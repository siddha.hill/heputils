## Makefile for HEPUtils
## Execute as 'make FASTJET_PREFIX=/path/to/fj', 'make pyext', 'make install', 'make clean'

VERSION := 1.4.0
CXX := g++ -std=c++14
CXXFLAGS := -O2

.PHONY = install uninstall dist clean distclean test pyext


test:
	$(warning No default make target: use 'make install' to install HEPUtils headers)

pyext: heputils.i
	@if (which swig > /dev/null); then \
      swig -python -c++ -cpperraswarn -Iinclude heputils.i && \
      $(CXX) $(CXXFLAGS) -shared -fPIC heputils_wrap.cxx -o _heputils.so -Iinclude `python-config --includes`; \
	else echo "swig is not available: not building Python wrapper" 1>&2; fi

ifndef PREFIX
install:
	$(error $$PREFIX is not set: run like 'make install PREFIX=/path/to/hepmc/install/area')
else
install:
	mkdir -p $(PREFIX)
	cp -r include $(PREFIX)
	test -e _heputils.so && PYV=`python --version 2>&1 | sed 's/Python \(.*\)\..*/python\1/'` && \
      PYDIR=$(PREFIX)/lib/$$PYV/site-packages && mkdir -p $$PYDIR && cp heputils.py _heputils.so $$PYDIR || true
endif


ifndef PREFIX
uninstall:
	$(error $$PREFIX is not set: run like 'make uninstall PREFIX=/path/to/hepmc/install/area')
else
uninstall:
	rm -rf $(PREFIX)/include/HEPUtils
endif


dist:
	tar czf HEPUtils-$(VERSION).tar.gz README TODO ChangeLog Makefile include

clean:
	rm -f heputils.py heputils_wrap.cxx _heputils.so

distclean: clean
	rm -f HEPUtils-$(VERSION).tar.gz
